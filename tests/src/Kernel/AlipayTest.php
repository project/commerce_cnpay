<?php

namespace Drupal\Tests\commerce_cnpay\Kernel;

use Drupal\commerce_cnpay\AlipayClient;
use Drupal\commerce_payment\Entity\PaymentGateway;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class AlipayTest extends CnpayTestBase {

  /**
   * The Alipay Page payment gateway.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $pageGateway;

  /**
   * The Alipay Page payment gateway plugin.
   *
   * @var \Drupal\commerce_cnpay_test\Plugin\Commerce\PaymentGateway\AlipayTestPage
   */
  protected $pageGatewayPlugin;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Creates a WeChat Native payment gateway.
    $this->pageGateway = PaymentGateway::create($this->getGatewayConfiguration(TRUE));
    $this->pageGateway->save();
    $this->pageGatewayPlugin = $this->pageGateway->getPlugin();
  }

  /**
   * Tests prepayPayment().
   */
  public function testPrepayPayment() {
    $payment = $this->createPayment();
    $prepay_response = $this->pageGatewayPlugin->prepayPayment($payment, [
      'return_url' => 'http://localhost/payment/return/test_alipay_page',
    ]);
    $this->assertEquals('http://localhost/payment/notify/test_alipay_page', $prepay_response['notify_url']);
    $this->assertEquals('http://localhost/payment/return/test_alipay_page', $prepay_response['return_url']);
  }

  /**
   * Tests prepayPayment() with amount change.
   */
  public function testPrepayPaymentWithAmountChange() {
    $config = $this->getGatewayConfiguration(TRUE)['configuration'];
    $client = new AlipayClient($config['app_id'], $config['app_private_key_path'], $config['alipay_public_key_path'], FALSE);
    $this->pageGatewayPlugin->setClient($client);

    // Prepay payment1.
    $payment1 = $this->createPayment();
    $prepay_response = $this->pageGatewayPlugin->prepayPayment($payment1);
    $biz_content = json_decode($prepay_response['biz_content'], TRUE);
    $this->assertEquals('1.00', $biz_content['total_amount']);
    $this->assertFalse($payment1->isNew(), 'Payment1 is saved.');

    // Prepay payment2 with changed amount for same order.
    $payment2 = $this->createPayment();
    $payment2->setAmount($payment1->getAmount()->multiply(10));
    $prepay_response = $this->pageGatewayPlugin->prepayPayment($payment2);
    $biz_content = json_decode($prepay_response['biz_content'], TRUE);
    $this->assertEquals('10.00', $biz_content['total_amount'], 'Payment amount is changed.');

    // Assert payment2.
    $this->assertEquals($payment1->id(), $payment2->id(), 'Payment2 points to payment1 since payment amount does not matter.');
    $this->assertEquals('authorization', $payment2->getState()->value);
    $this->assertEquals('', $payment2->getRemoteId());
    $this->assertEquals(NULL, $payment2->getRemoteState());

    // Assert payment1 is the only payment for the order.
    $payments =  $this->paymentStorage->loadMultipleByOrder($this->cart);
    $this->assertEquals(1, count($payments));
    $payment = reset($payments);
    $this->assertEquals($payment1->id(), $payment->id(), 'Payment1 is the only payment.');
    $this->assertEquals('10.00', $payment->getAmount()->getNumber(), 'Payment amount is up to date.');
  }

  /**
   * Tests refund() API.
   */
  public function testRefundApi() {
    $http_client = $this->getMockBuilder(Client::class)
      ->setMethods(['post'])
      ->getMock();
    $http_client->expects($this->once())
      ->method('post')
      ->willReturn(new Response(200, [], json_encode([
        'alipay_trade_refund_response' => [
          'code' => '10000',
          'msg' => 'Success',
          'buyer_logon_id' => '908***@qq.com',
          'buyer_user_id' => '2088101117955611',
          'fund_change' => 'Y',
          'gmt_refund_pay' => '2017-11-12 21:45:57',
          'out_trade_no' => '123',
          'refund_fee' => '1.0',
          'send_back_fee' => '0.00',
          'trade_no' => '2017123',
        ],
        'sign' => 'abcdefg123456789',
      ])));
    $config = $this->getGatewayConfiguration(TRUE)['configuration'];
    $client = new AlipayClient($config['app_id'], $config['app_private_key_path'], $config['alipay_public_key_path'], FALSE, $http_client);

    $result = $client->refund([
      'trade_no' => '2017123',
      'refund_amount' => '1.0',
      // 'refund_reason' => '',
      'out_request_no' => '123',
    ]);
    $this->assertEquals([
      'code' => '10000',
      'msg' => 'Success',
      'buyer_logon_id' => '908***@qq.com',
      'buyer_user_id' => '2088101117955611',
      'fund_change' => 'Y',
      'gmt_refund_pay' => '2017-11-12 21:45:57',
      'out_trade_no' => '123',
      'refund_fee' => '1.0',
      'send_back_fee' => '0.00',
      'trade_no' => '2017123',
    ], $result);
  }

  /**
   * Tests transfer api.
   */
  public function testTransferApi() {
    $http_client = $this->getMockBuilder(Client::class)
      ->setMethods(['post'])
      ->getMock();
    $http_client->expects($this->once())
      ->method('post')
      ->willReturn(new Response(200, [], json_encode([
        'alipay_fund_trans_toaccount_transfer_response' => [
          'code' => '10000',
          'msg' => 'Success',
          'out_biz_no' => '2018123',
          'order_id' => '20160627110070001502260006780837',
          'pay_date' => '2018-01-01 08:08:08',
        ],
        'sign' => 'abcdefg123456789',
      ])));
    $config = $this->getGatewayConfiguration(TRUE)['configuration'];
    $client = new AlipayClient($config['app_id'], $config['app_private_key_path'], $config['alipay_public_key_path'], FALSE, $http_client);

    $result = $client->transfer([
      'out_biz_no' => '2018123',
      'payee_type' => 'ALIPAY_LOGONID',
      'payee_account' => 'abc@sina.com',
      'amount' => '10.00',
      'payee_real_name' => '张三',
      'remark' => '转账测试',
    ]);
    $this->assertEquals([
      'code' => '10000',
      'msg' => 'Success',
      'out_biz_no' => '2018123',
      'order_id' => '20160627110070001502260006780837',
      'pay_date' => '2018-01-01 08:08:08',
    ], $result);
  }

  /**
   * Gets payment gateway configuration for NATIVE or JSAPI.
   *
   * @param bool $page
   *   Whether the configuration is for Page pay.
   *
   * @return array
   *   The configuration array.
   */
  protected function getGatewayConfiguration($page) {
    $cert_folder = drupal_get_path('module', 'commerce_cnpay') . '/tests/cert';
    return [
      'id' => $page ? 'test_alipay_page' : 'test_alipay_wap',
      'plugin' => $page ? 'alipay_test_page' : 'alipay_test_wap',
      'configuration' => [
        'app_id' => '2017111111111111',
        'seller_id' => '2088123456789012',
        'app_private_key_path' => $cert_folder . '/apiclient_key.pem',
        'alipay_public_key_path' => $cert_folder . '/apiclient_cert.pem',
        'display_label' => 'Alipay',
        'mode' => 'live',
        'payment_method_types' => [
          'credit_card',
        ],
      ],
    ];
  }

  /**
   * Creates a payment.
   *
   * Note: do not save the payment.
   *
   * @param bool $page
   *   Whether the configuration is for Page.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected function createPayment($page = TRUE) {
    $payment = $this->paymentStorage->create([
      'state' => 'new',
      'amount' => $this->cart->getTotalPrice(),
      'payment_gateway' => $page ? 'test_alipay_page' : 'test_alipay_wap',
      'order_id' => $this->cart->id(),
    ]);
    return $payment;
  }

}
