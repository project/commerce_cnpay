<?php

namespace Drupal\commerce_cnpay_test;

use EasyWeChat\Payment\Application;
use EasyWeChat\Payment\Transfer;

class WeChatTestTransferClient extends Transfer\Client {

  public function __construct() {
    parent::__construct(new Application());
  }

  public function queryBalanceOrder(string $partnerTradeNo) {
  }

  public function toBalance(array $params) {
  }

  public function queryBankCardOrder(string $partnerTradeNo) {
  }

  public function toBankCard(array $params) {
  }

}
