<?php

namespace Drupal\commerce_cnpay_test;

use EasyWeChat\Payment\Application;

/**
 * Used to mock objects for magic __call() method.
 */
class WeChatTestClient extends Application {

  public function __construct(WeChatTestOrderClient $order_client = NULL, WeChatTestTransferClient $transfer_client = NULL) {
    parent::__construct();

    if ($order_client) {
      $this['order'] = function ($app) use ($order_client) {
        return $order_client;
      };
    }

    if ($transfer_client) {
      $this['transfer'] = function ($app) use ($transfer_client) {
        return $transfer_client;
      };
    }
  }

}
