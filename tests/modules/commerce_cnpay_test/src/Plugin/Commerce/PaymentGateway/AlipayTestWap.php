<?php

namespace Drupal\commerce_cnpay_test\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_cnpay\Plugin\Commerce\PaymentGateway\AlipayWap;

/**
 * Provides the Alipay Test (Wap) payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "alipay_test_wap",
 *   label = "Alipay Test (Wap)",
 *   display_label = "Alipay",
 *   api = "alipay.trade.wap.pay",
 *   product_code = "FAST_INSTANT_TRADE_PAY",
 * )
 */
class AlipayTestWap extends AlipayWap {

  use TestGatewayTrait;

}
