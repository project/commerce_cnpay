<?php

namespace Drupal\commerce_cnpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_cnpay\TransferInterface;

/**
 * Defines the interface for gateways which support transferring funds.
 */
interface SupportsTransfersInterface {

  /**
   * Transfers funds to a receiver.
   *
   * Note: the given transfer might be changed by settings a remote id or
   * remote state, and you need to save the change manually.
   *
   * @param \Drupal\commerce_cnpay\TransferInterface $transfer
   *   The funds transfer.
   *
   * @return array
   *   The response array.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the transaction fails for any reason.
   */
  public function transferFunds(TransferInterface $transfer);

  /**
   * Queries a funds transfer.
   *
   * @param \Drupal\commerce_cnpay\TransferInterface $transfer
   *   The funds transfer.
   *
   * @return array
   *   The response array.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the transaction fails for any reason.
   */
  public function queryTransfer(TransferInterface $transfer);

}
