<?php

namespace Drupal\commerce_cnpay\Plugin\Commerce\PaymentGateway;

use EasyWeChat\Payment\Application;

/**
 * Provides the interface for the WeChat payment gateway.
 */
interface WeChatPaymentGatewayInterface extends CNPaymentGatewayInterface {

  /**
   * Gets the client to perform WeChat Pay API calls.
   *
   * @return \EasyWeChat\Payment\Application
   *   The WeChat Pay client.
   */
  public function getClient();

  /**
   * Sets the client to perform WeChat Pay API calls.
   *
   * @param \EasyWeChat\Payment\Application $client
   *   The WeChat Pay client.
   */
  public function setClient(Application $client);

  /**
   * Whether or not the openid is required.
   *
   * @return boolean
   *   TRUE if the trade type is JSAPI, FALSE otherwise.
   */
  public function requiresOpenid();

}
