<?php

namespace Drupal\commerce_cnpay;

/**
 * Value object representing funds transfer.
 */
class Transfer implements TransferInterface {

  /**
   * The transfer id.
   *
   * @var string
   */
  protected $id;

  /**
   * The funds amount.
   *
   * @var \Drupal\commerce_price\Element\Price
   */
  protected $amount;

  /**
   * The funds receiver identifier.
   *
   * @var string
   */
  protected $receiver;

  /**
   * The transfer note.
   *
   * @var string
   */
  protected $note = '转账';

  /**
   * The funds receiver real name.
   *
   * @var string|null
   */
  protected $realName;

  /**
   * The transfer trade remote ID.
   *
   * @var string|null
   */
  protected $remoteId;

  /**
   * Constructs a new Transfer object.
   *
   * @param array $values
   *   The values.
   */
  public function __construct(array $values) {
    foreach (['id', 'amount', 'receiver'] as $required_property) {
      if (empty($values[$required_property])) {
        throw new \InvalidArgumentException(sprintf('Missing required property "%s".', $required_property));
      }
    }

    $this->id = $values['id'];
    $this->amount = $values['amount'];
    $this->receiver = $values['receiver'];
    if (!empty($values['real_name'])) {
      $this->realName = $values['real_name'];
    }
    if (!empty($values['note'])) {
      $this->note = $values['note'];
    }
    if (!empty($values['remote_id'])) {
      $this->remoteId = $values['remote_id'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    return $this->amount;
  }

  /**
   * {@inheritdoc}
   */
  public function getReceiver() {
    return $this->receiver;
  }

  /**
   * {@inheritdoc}
   */
  public function getRealName() {
    return $this->realName;
  }

  /**
   * {@inheritdoc}
   */
  public function getNote() {
    return $this->note;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->remoteId;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($remote_id) {
    $this->remoteId = $remote_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    // Do nothing for the value object.
  }

}
