<?php

namespace Drupal\commerce_cnpay;

/**
 * The funds transfer interface.
 */
interface TransferInterface {

  /**
   * Transfer is awaiting complete.
   */
  const PENDING = 'PENDING';

  /**
   * Transfer is successful.
   */
  const SUCCESS = 'SUCCESS';

  /**
   * Transfer is failed.
   */
  const FAILURE = 'FAILURE';

  /**
   * Gets the transaction ID.
   *
   * @return string
   *   The transaction id.
   */
  public function id();

  /**
   * Gets the funds amount.
   *
   * @return \Drupal\commerce_price\Price
   *   The funds amount.
   */
  public function getAmount();

  /**
   * Gets the funds receiver.
   *
   * @return string
   *   The funds receiver identifier in payment gateway provider.
   */
  public function getReceiver();

  /**
   * Gets the receiver real name.
   *
   * @return string|null
   *   The funds receiver real name for verification, NULL if not defined.
   */
  public function getRealName();

  /**
   * Gets the transaction note.
   *
   * @return string
   *   The transaction note.
   */
  public function getNote();

  /**
   * Gets the remote payment ID.
   *
   * @return string
   *   The remote payment ID.
   */
  public function getRemoteId();

  /**
   * Sets the remote payment ID.
   *
   * @param string $remote_ID
   *   The remote payment ID.
   *
   * @return $this
   */
  public function setRemoteId($remote_ID);

  /**
   * Gets the remote payment state.
   *
   * @return string
   *   The remote payment state.
   */
  public function getRemoteState();

  /**
   * Sets the remote payment state.
   *
   * @param string $remote_state
   *   The emote payment state.
   *
   * @return $this
   */
  public function setRemoteState($remote_state);

  /**
   * Saves the object instance.
   */
  public function save();

}
